import pytest

def increment(x):
  return x + 1

def test_increment():
  assert increment(3) == 4

if __name__ == '__main__': 
  pytest.main(['example_pytest.py'])
