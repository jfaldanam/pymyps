import unittest

def increment(x):
  return x + 1

class TestIncrement(unittest.TestCase):
  def test_increment(self):
    self.assertEqual(increment(3), 4)
    
if __name__ == '__main__':
  unittest.main(exit=False)
