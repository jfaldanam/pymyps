from calculator import Calculator
import pytest

@pytest.fixture(scope='session')
def calculator():
  return Calculator()

def test_suma(calculator):
  assert calculator.suma(2, 3) == 5
  
def test_mult(calculator):
  assert calculator.mult(2,2) == 4
